# Sort

- Для юнит-тестов использовался фреймворк [Catch](https://github.com/catchorg/Catch2), который представлен одним заголовочным файлом.
Таким образом тесты компилируются без стороних зависимостей.
- Также я описал как бы проводил [системное тестирование](https://docs.google.com/spreadsheets/d/1_LyTeZsIVogoDK5ZdSHLtaZV5WyjXI6gcLcEOaewM8Q/edit?usp=sharing), используя ТЗ в качестве требований.
