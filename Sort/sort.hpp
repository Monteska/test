﻿#pragma once

#include <iostream>
#include <vector>
#include <functional>

enum class Color
{
	Red,
	Green,
	Blue
};
std::ostream& operator<< (std::ostream &out, const Color &color);

class Object
{
private:
	Color _color;
public:
	explicit Object(Color color);
	Color getColor() const;

	friend bool operator== (const Object &objl, const Object &objr);
};

// Генерирует функцию сравнения из порядка цветов в параметрах (с1 < c2 < c3)
const std::function<bool(const Object&, const Object&)> pattern(Color c1, Color c2, Color c3);

// Генерирует функцию сравнения из порядка цветов в параметрах (с1 < c2)
const std::function<bool(const Object&, const Object&)> pattern(Color c1, Color c2);

// Соортирует объекты (пузырьковая соортировка) сравнивая их полученой функцией сравнения
void sortObjects(std::vector<Object> &objects,
	const std::function<bool(const Object&, const Object&)> &lessThan);

// Преобразует полученную строку в вектор объектов
std::vector<Object> inputObjects(const std::string& strObjects);

// Преобразует полученную строку в функцию сравнения через метод pattern
const std::function<bool(const Object&, const Object&)> inputPattern(const std::string& strPattern);
