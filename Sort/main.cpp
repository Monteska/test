﻿#include <iostream>
#include <string>
#include <vector>

#include "sort.hpp"

int main(int argc, char *argv[])
{
	std::cout << "This program for sorting objects marked colors" << std::endl
			  << "Object marked one of three colors: red, green or blue (R, G or B)" << std::endl
			  << "Example input object: RGBRGBRGB (any other character will be unknown color)" << std::endl
			  << "Sorting pattern is colors sequence (c1 < c2 < c3)" << std::endl
			  << "Example input pattern: RGB (any other character will be unknown color) \n" << std::endl;

	while (true)
	{
		try
		{
			std::string strObjects;
			std::cout << "Enter objects collection: " << std::endl;
			std::getline(std::cin, strObjects);

			std::vector<Object> objects = inputObjects(strObjects);

			std::string strPattern;
			std::cout << "Enter sorting pattern: " << std::endl;
			std::getline(std::cin, strPattern);

			sortObjects(objects, inputPattern(strPattern));

			std::cout << "Sorted objects: " << std::endl;
			for (auto obj : objects)
				std::cout << obj.getColor();
			std::cout << std::endl << std::endl;
		}
		catch (std::runtime_error e)
		{
			std::cerr << e.what() << std::endl << std::endl;
			std::cin.clear();
		}
	}

	return 0;
}
