﻿#include "sort.hpp"

#include <stdexcept>

std::ostream& operator<< (std::ostream &out, const Color &color)
{
	if (color == Color::Red)
		out << "R";
	if (color == Color::Green)
		out << "G";
	if (color == Color::Blue)
		out << "B";
	return out;
}

Object::Object(Color color)
{
	this->_color = color; 
}

Color Object::getColor() const
{
	return _color;
}

bool operator==(const Object &objl, const Object &objr)
{
	return (objl.getColor() == objr.getColor());
}

const std::function<bool(const Object&, const Object&)> pattern(Color c1, Color c2, Color c3)
{
	if (c1 == c2 || c2 == c3 || c1 == c3)
		throw std::runtime_error("Invalid pattern");

	return [c1, c2, c3](const Object& objl, const Object& objr) -> bool
	{
		// Проверка: можно ли сравнить эти цвета 
		// (поскольку в аргументах может оказаться незнакомый цвет)
		if ((objl.getColor() != c1 && objl.getColor() != c2 && objl.getColor() != c3) ||
			(objr.getColor() != c1 && objr.getColor() != c2 && objr.getColor() != c3))
			throw std::runtime_error("Invalid pattern");

		if (objl.getColor() == objr.getColor())
			return false;
		else if (objl.getColor() == c1)
			return true;
		else if (objl.getColor() == c2 && objr.getColor() == c3)
			return true;
		else
			return false;
	};
}

const std::function<bool(const Object&, const Object&)> pattern(Color c1, Color c2)
{
	if (c1 == c2)
		throw std::runtime_error("Invalid pattern");

	return [c1, c2](const Object& objl, const Object& objr) -> bool
	{
		// Проверка: можно ли сравнить эти цвета 
		// (поскольку в аргументах может оказаться незнакомый цвет)
		if ((objl.getColor() != c1 && objl.getColor() != c2) ||
			(objr.getColor() != c1 && objr.getColor() != c2))
			throw std::runtime_error("Invalid pattern");

		if (objl.getColor() == objr.getColor())
			return false;
		else if (objl.getColor() == c1)
			return true;
		else
			return false;
	};
}

void sortObjects(std::vector<Object> &objects,
				 const std::function<bool(const Object&, const Object&)> &lessThan)
{
	for (size_t c = 1; c < objects.size(); c++)
	{
		for (size_t i = 0; i < objects.size() - c; i++)
		{
			if (lessThan(objects[i + 1], objects[i]))
				std::swap(objects[i + 1], objects[i]);
		}
	}
}

std::vector<Object> inputObjects(const std::string& strObjects)
{
	std::vector<Object> objects;
	for (auto obj : strObjects)
	{
		if (obj == 'R')
			objects.push_back(Object(Color::Red));
		else if (obj == 'G')
			objects.push_back(Object(Color::Green));
		else if (obj == 'B')
			objects.push_back(Object(Color::Blue));
		else if (obj != ' ')
			throw std::runtime_error("Unknown color in collection");
	}
	return objects;
}

const std::function<bool(const Object&, const Object&)> inputPattern(const std::string& strPattern)
{
	Color c[3];
	size_t count = 0;

	for (auto color : strPattern)
	{
		if (color == 'R')
		{
			c[count] = Color::Red;
			count++;
		}
		else if (color == 'G')
		{
			c[count] = Color::Green;
			count++;
		}
		else if (color == 'B')
		{
			c[count] = Color::Blue;
			count++;
		}
		else if (color != ' ')
			throw std::runtime_error("Unknown color in pattern");
	}

	if (count == 2)
		return pattern(c[0], c[1]);
	else if (count == 3)
		return pattern(c[0], c[1], c[2]);
	else
		throw std::runtime_error("Invalid pattern");
}
