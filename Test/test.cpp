﻿#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../Sort/sort.hpp"
#include "../Sort/sort.cpp"

TEST_CASE("ValidPattern_UniqueArguments")
{
	REQUIRE_NOTHROW(pattern(Color::Red, Color::Green, Color::Blue));
	REQUIRE_NOTHROW(pattern(Color::Blue, Color::Green, Color::Red));
	REQUIRE_NOTHROW(pattern(Color::Green, Color::Red, Color::Blue));
	REQUIRE_NOTHROW(pattern(Color::Red, Color::Green));
	REQUIRE_NOTHROW(pattern(Color::Blue, Color::Green));
	REQUIRE_NOTHROW(pattern(Color::Green, Color::Red));
}
TEST_CASE("InvalidPattern_DublicateArguments")
{
	REQUIRE_THROWS(pattern(Color::Red, Color::Red, Color::Green));
	REQUIRE_THROWS(pattern(Color::Blue, Color::Green, Color::Blue));
	REQUIRE_THROWS(pattern(Color::Green, Color::Green, Color::Green));
	REQUIRE_THROWS(pattern(Color::Red, Color::Red));
	REQUIRE_THROWS(pattern(Color::Blue, Color::Blue));
	REQUIRE_THROWS(pattern(Color::Green, Color::Green));
}
TEST_CASE("CorrectCompareFromPattern_3UniqueArguments")
{
	auto lessThan = pattern(Color::Blue, Color::Red, Color::Green);

	REQUIRE(lessThan(Object(Color::Red), Object(Color::Green)));
	REQUIRE(lessThan(Object(Color::Blue), Object(Color::Red)));
	REQUIRE(lessThan(Object(Color::Blue), Object(Color::Green)));
	REQUIRE_FALSE(lessThan(Object(Color::Green), Object(Color::Red)));
	REQUIRE_FALSE(lessThan(Object(Color::Red), Object(Color::Blue)));
	REQUIRE_FALSE(lessThan(Object(Color::Green), Object(Color::Blue)));
}
TEST_CASE("CorrectCompareFromPattern_2UniqueArguments")
{
	auto lessThan = pattern(Color::Green, Color::Blue);

	REQUIRE(lessThan(Object(Color::Green), Object(Color::Blue)));
	REQUIRE_FALSE(lessThan(Object(Color::Blue), Object(Color::Green)));
}

TEST_CASE("CorrectCompareFromPattern_3DublicateArguments")
{
	auto lessThanRBG = pattern(Color::Red, Color::Blue, Color::Green);
	auto lessThanGRB = pattern(Color::Green, Color::Red, Color::Blue);
	auto lessThanBGR = pattern(Color::Blue, Color::Green, Color::Red);

	REQUIRE_FALSE(lessThanRBG(Object(Color::Red), Object(Color::Red)));
	REQUIRE_FALSE(lessThanRBG(Object(Color::Green), Object(Color::Green)));
	REQUIRE_FALSE(lessThanRBG(Object(Color::Blue), Object(Color::Blue)));
	REQUIRE_FALSE(lessThanGRB(Object(Color::Green), Object(Color::Green)));
	REQUIRE_FALSE(lessThanGRB(Object(Color::Red), Object(Color::Red)));
	REQUIRE_FALSE(lessThanGRB(Object(Color::Blue), Object(Color::Blue)));
	REQUIRE_FALSE(lessThanBGR(Object(Color::Green), Object(Color::Green)));
	REQUIRE_FALSE(lessThanBGR(Object(Color::Red), Object(Color::Red)));
	REQUIRE_FALSE(lessThanBGR(Object(Color::Blue), Object(Color::Blue)));
}

TEST_CASE("CorrectCompareFromPattern_2DublicateArguments")
{
	auto lessThanBG = pattern(Color::Blue, Color::Green);
	auto lessThanRB = pattern(Color::Red, Color::Blue);
	auto lessThanBR = pattern(Color::Blue, Color::Red);

	REQUIRE_FALSE(lessThanBG(Object(Color::Blue), Object(Color::Blue)));
	REQUIRE_FALSE(lessThanBG(Object(Color::Green), Object(Color::Green)));
	REQUIRE_FALSE(lessThanRB(Object(Color::Red), Object(Color::Red)));
	REQUIRE_FALSE(lessThanRB(Object(Color::Blue), Object(Color::Blue)));
	REQUIRE_FALSE(lessThanBR(Object(Color::Red), Object(Color::Red)));
	REQUIRE_FALSE(lessThanBR(Object(Color::Blue), Object(Color::Blue)));
}

TEST_CASE("CorrectCompareFromPattern_UnknownColor")
{
	auto lessThan = pattern(Color::Red, Color::Green); // Blue неизвестен фукнции

	REQUIRE_THROWS(lessThan(Object(Color::Red), Object(Color::Blue)));
	REQUIRE_THROWS(lessThan(Object(Color::Blue), Object(Color::Green)));
	REQUIRE_THROWS(lessThan(Object(Color::Blue), Object(Color::Blue)));
}

TEST_CASE("CorrectSortObjects_EmptyCollection")
{
	auto compare = pattern(Color::Red, Color::Green, Color::Blue);
	std::vector<Object> objects;

	sortObjects(objects, compare);

	REQUIRE(objects.size() == 0);
}

TEST_CASE("CorrectSortObjects_OneObjectInCollection")
{
	auto compare = pattern(Color::Green, Color::Blue, Color::Red);
	std::vector<Object> objects{ Object(Color::Blue) };
	auto sortedObjects = objects;

	sortObjects(objects, compare);

	REQUIRE(objects == sortedObjects);
}

TEST_CASE("CorrectSortObjects_SortedCollection")
{
	auto compare = pattern(Color::Blue, Color::Red, Color::Green);
	std::vector<Object> objectsBRG
	{
		Object(Color::Blue), Object(Color::Blue), Object(Color::Blue),
		Object(Color::Red), Object(Color::Red), Object(Color::Red),
		Object(Color::Green), Object(Color::Green), Object(Color::Green)
	};
	auto sortedObjectsBRG = objectsBRG;
	std::vector<Object> objectsRG
	{
		Object(Color::Red), Object(Color::Red),
		Object(Color::Green), Object(Color::Green)
	};
	auto sortedObjectsRG = objectsRG;
	std::vector<Object> objectsB
	{
		Object(Color::Blue), Object(Color::Blue),
		Object(Color::Blue), Object(Color::Blue)
	};
	auto sortedObjectsB = objectsB;

	sortObjects(objectsBRG, compare);
	sortObjects(objectsRG, compare);
	sortObjects(objectsB, compare);

	REQUIRE(objectsBRG == sortedObjectsBRG);
	REQUIRE(objectsRG == sortedObjectsRG);
	REQUIRE(objectsB == sortedObjectsB);
}

TEST_CASE("CorrectSortObjects_UnsortedCollection")
{
	auto compare = pattern(Color::Red, Color::Green, Color::Blue);

	std::vector<Object> objectsRBR
	{
		Object(Color::Red), Object(Color::Blue), Object(Color::Red)
	};
	std::vector<Object> sortedObjectsRRB
	{
		Object(Color::Red), Object(Color::Red), Object(Color::Blue)
	};
	std::vector<Object> objectsBGRBGRBGR
	{
		Object(Color::Blue), Object(Color::Green), Object(Color::Red),
		Object(Color::Blue), Object(Color::Green), Object(Color::Red),
		Object(Color::Blue), Object(Color::Green), Object(Color::Red)
	};
	std::vector<Object> sortedObjectsRRRGGGBBB
	{
		Object(Color::Red), Object(Color::Red), Object(Color::Red),
		Object(Color::Green), Object(Color::Green), Object(Color::Green),
		Object(Color::Blue), Object(Color::Blue), Object(Color::Blue)
	};
	std::vector<Object> objectsGGBBRR
	{
		Object(Color::Green), Object(Color::Green),
		Object(Color::Blue), Object(Color::Blue),
		Object(Color::Red), Object(Color::Red)
	};
	std::vector<Object> sortedObjectsRRGGBB
	{
		Object(Color::Red), Object(Color::Red),
		Object(Color::Green), Object(Color::Green),
		Object(Color::Blue), Object(Color::Blue)
	};

	sortObjects(objectsRBR, compare);
	sortObjects(objectsBGRBGRBGR, compare);
	sortObjects(objectsGGBBRR, compare);

	REQUIRE(objectsRBR == sortedObjectsRRB);
	REQUIRE(objectsBGRBGRBGR == sortedObjectsRRRGGGBBB);
	REQUIRE(objectsGGBBRR == sortedObjectsRRGGBB);
}

TEST_CASE("ErrorSortObjects_UnknownColorInCollection")
{
	auto compare = pattern(Color::Blue, Color::Green);
	std::vector<Object> objectsRGB
	{
		Object(Color::Red), Object(Color::Green), Object(Color::Blue)
	};

	REQUIRE_THROWS(sortObjects(objectsRGB, compare));
}

TEST_CASE("CorrectInputObjects_ValidCollection")
{
	std::string strObjectsRGB = "RGB";
	std::string strObjectsRRBB = "RRBB";
	std::string strObjectsRRBBWithSpaces = " RR   B B ";
	std::string strObjectsEmpty = "";
	std::string strObjectsSpaces= "    ";

	std::vector<Object> objectsRGB
	{
		Object(Color::Red), Object(Color::Green), Object(Color::Blue)
	};
	std::vector<Object> objectsRRBB
	{
		Object(Color::Red), Object(Color::Red),
		Object(Color::Blue), Object(Color::Blue)
	};
	std::vector<Object> objectsEmpty;

	REQUIRE_NOTHROW(inputObjects(strObjectsRGB));
	REQUIRE_NOTHROW(inputObjects(strObjectsRRBB));
	REQUIRE_NOTHROW(inputObjects(strObjectsRRBBWithSpaces));
	REQUIRE_NOTHROW(inputObjects(strObjectsEmpty));
	REQUIRE_NOTHROW(inputObjects(strObjectsSpaces));

	REQUIRE(inputObjects(strObjectsRGB) == objectsRGB);
	REQUIRE(inputObjects(strObjectsRRBB) == objectsRRBB);
	REQUIRE(inputObjects(strObjectsRRBBWithSpaces) == objectsRRBB);
	REQUIRE(inputObjects(strObjectsEmpty) == objectsEmpty);
	REQUIRE(inputObjects(strObjectsSpaces) == objectsEmpty);
}
TEST_CASE("CorrectInputObjects_InvalidCollection")
{
	std::string strObjectsWithUnknownChar = "RGBO";

	REQUIRE_THROWS(inputObjects(strObjectsWithUnknownChar));
}
TEST_CASE("CorrectInputPattern_ValidPattern")
{
	std::string strPatternRGB = "RGB";
	std::string strPatternRGBWithSpaces = "R G B";
	std::string strPatternRB = "BG";

	REQUIRE_NOTHROW(inputPattern(strPatternRGB));
	REQUIRE_NOTHROW(inputPattern(strPatternRGBWithSpaces));
	REQUIRE_NOTHROW(inputPattern(strPatternRB));

}
TEST_CASE("CorrectInputPattern_InvalidPattern")
{
	std::string strPatternRRR = "RRR";
	std::string strPatternWithUnknownChar = "RO";
	std::string strPatternEmpty = "";

	REQUIRE_THROWS(inputPattern(strPatternRRR));
	REQUIRE_THROWS(inputPattern(strPatternWithUnknownChar));
	REQUIRE_THROWS(inputPattern(strPatternEmpty));
}
